/*! Main */
jQuery(document).ready(function($) {
    // Pega #id da url e ativa a tab automaticamente
    $(function () {
        var tab = window.location.hash.substr(1);
        $('nav[role="tablist"] a').removeClass('active');
        $('nav[role="tablist"] a[href="#' + tab + '"]').tab('show').addClass('active');

        $('nav[role="tablist"] a').click(function(e) {
            e.preventDefault();
            $('nav[role="tablist"] a').removeClass('active');
            $(this).tab('show').addClass('active');
            // $('#bg').removeClass().addClass( 'bg fade in ' + $(this).attr( "data-bg" ));
        });
    });

    // Fixa navbar ao ultrapassa-lo
    var distance = $('#navbar-main').offset().top,
        $window = $(window);

    $window.scroll(function() {
        if( $window.scrollTop() >= distance) {
            $('#navbar-main').removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
        }
        else{
            $('#navbar-main').removeClass('navbar-fixed-top');
        }
    });
 // -------------- JS da programação dos eventos -----------------------
 
    $( window ).load(function() {
        $('.choice-activity').hide();
    });

    $('.choice-course button[type="button"]').on('click', function(){
        $('.choice-course button[type="button"]').removeClass('active');
        $(this).addClass('active');
    
        $('.choice-activity').fadeIn("slow");
    
        var atividade = $('.choice-activity button[type="button"].active');
        if(atividade.val() == undefined){
            atividade = 'palestra-visita';
        }
        else{
            var atividade = atividade.val().substr(1, 100);
        }
        var classe_atividade = '.evento-'+atividade;
        
        var classe = $(this).val();
        var data = $('ul#myTabs li.active a').attr('id').substr(4, 4);
        var data_classe = '.data-'+data;
        
        var qtd_na_data = parseInt($(classe+data_classe).length);
        var classe_vazio = '.vazio-'+data;
        
        var qtd_minicurso = parseInt($('.evento-minicurso'+classe).length);

        $(classe_atividade).hide();
        $(classe_atividade+classe).show();
    
        $('.vazio').addClass('hide');
        if(qtd_na_data < 1){
            $(classe_vazio).removeClass('hide');
        }
        $('.vazio-minicurso').addClass('hide');
        if(qtd_minicurso < 1){
            $('.vazio-minicurso').removeClass('hide');
        }
    });

    $('.choice-activity button[type="button"]').on('click', function(){
        $('.choice-activity button[type="button"]').removeClass('active');
        $(this).addClass('active');
    
        var classe = $(this).val();
        var atividade = classe.substr(1, 100);
        var classe_atividade = '.evento-'+atividade;
        var curso = $('.choice-course button[type="button"].active').val();
        var classe_curso = classe_atividade+curso;
    
        if(classe == '.palestra-visita'){
            $('.minicurso').addClass('hide');
        $('.palestra-visita').removeClass('hide');
        }
        else{
            $('.minicurso').removeClass('hide');
            $('.palestra-visita').addClass('hide');
        }
    
        var qtd = parseInt($(classe_curso).length);
        var classe_vazio = '.vazio-minicurso';
    
        $(classe_atividade).hide();
        $('.evento-'+atividade).hide();
        $('.evento-'+atividade+curso).show();
    
        $(classe_vazio).addClass('hide');
        if(qtd < 1){
            $(classe_vazio).removeClass('hide');
        }
    });

    $('ul#myTabs li a').on('click', function(){
        var data = $(this).attr('id').substr(4, 4);
        var data_classe = '.data-'+data;
        var classe = $('.choice-course button[type="button"].active').val();
        var qtd_na_data = parseInt($(classe+data_classe).length);
        var classe_vazio = '.vazio-'+data;
    
        $('.vazio').addClass('hide');
        if(qtd_na_data < 1){
            $(classe_vazio).removeClass('hide');
        }
    });

// ------------

/* ---- .ACTIVE ON BTN GROUPS ----*/ 
    $('.choice-course button[type="button"]').click(function(){
        $('.choice-course button[type="button"]').removeClass("active");
        $(this).addClass("active");
    });
    $('.choice-activity button[type="button"]').click(function(){
        $('.choice-activity button[type="button"]').removeClass("active");
        $(this).addClass("active");
    });

// ////////////// JS Wally Home ///////////// //
    
// ////////////// JS Wally com parametro ///////////// //

});










