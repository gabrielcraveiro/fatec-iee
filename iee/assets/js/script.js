$(main);
    
function main(){
    $('#xls').css('display', 'none');
    
    $('#btnManual').click(function(e){
        $('#xls').hide( 'fast' );
        $('#manual').show( 'slow' );
        $('#btnXls').removeClass('active');
        $(this).addClass('active');
    });
    
    $('#btnXls').click(function(e){
        $('#manual').hide( 'fast' );
        $('#xls').show( 'slow' );
        $('#btnManual').removeClass('active');
        $(this).addClass('active');
    });
    

    $('.salario1').mask('000.000,00');
    $('.salario2').mask('000.000,00');
    $('.jornada1').mask('00:00');
    $('.jornada2').mask('00:00');
    
    $('.siteEmpresa').mask('')
    
}