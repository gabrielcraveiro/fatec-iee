/*! Main */
jQuery(document).ready(function($) {

    // Pega #id da url e ativa a tab automaticamente
    $(function () {
        var tab = window.location.hash.substr(1);
        $('nav[role="tablist"] a').removeClass('active');
        $('nav[role="tablist"] a[href="#' + tab + '"]').tab('show').addClass('active');

        $('nav[role="tablist"] a').click(function(e) {
            e.preventDefault();
            $('nav[role="tablist"] a').removeClass('active');
            $(this).tab('show').addClass('active');
            // $('#bg').removeClass().addClass( 'bg fade in ' + $(this).attr( "data-bg" ));
        });
    });

    // Fixa navbar ao ultrapassa-lo
    var distance = $('#navbar-main').offset().top,
        $window = $(window);

    $window.scroll(function() {
        if ( $window.scrollTop() >= distance) {
            $('#navbar-main').removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
        } else {
            $('#navbar-main').removeClass('navbar-fixed-top');
        }
    });
});
