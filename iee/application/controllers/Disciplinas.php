<?php

    class Disciplinas extends CI_Controller {
        
        public function __construct(){
		    parent::__construct();
        }
        
        public function insereDisciplina(){
        	$this->load->database();
        	$this->load->model('Disciplinas_Model');
        	$this->load->library('PHPExcel/PHPExcel');
		    $objPHPExcel = $this->phpexcel;
		    $arquivo = FCPATH.'/disciplinas.xlsx';
		    $tipo = PHPExcel_IOFactory::identify($arquivo);
		    $objReader = PHPExcel_IOFactory::createReader($tipo);
		    $arquivo = $objReader->load($arquivo);
        	
        	$dados = array();
    		$dados['planilha'] = $arquivo->getSheet(0);
    		$max_row = $dados['planilha']->getHighestRow();
    		$max_col = $dados['planilha']->getHighestColumn();
		    
		  
		    for ($i = 2; $i <= $max_row; $i++) {
	            $linha = $dados['planilha']->rangeToArray("A$i:F$i", NULL, TRUE, FALSE);
		        $disciplina["cd_Disciplina"] = $linha[0][0];
		        $disciplina["ds_Disciplina"] = $linha[0][1];
		        $disciplina["ds_Classificacao"] = $linha[0][2];
		        $disciplina["ds_Ciclo"] = $linha[0][3];
		        $this->Disciplinas_Model->inserir($disciplina);
		       
		    }
		    
        }
      
    }


