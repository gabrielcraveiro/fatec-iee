<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresas extends CI_Controller {
    
	public function __construct(){
		parent::__construct();
		$this->load->model('Empresas_model');
		$this->load->library('pagination');
	}
	
	public function index(){
	    
		$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

		//Toda configuração de Paginação CodeIgniter
		$config['base_url'] = base_url().'empresas/index';
		$config['total_rows'] = $this->Empresas_model->contaRegistros();
		$config['per_page'] = '10';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = 'Próximo';
		$config['prev_link'] = 'Anterior';
		$this->pagination->initialize($config); 
		
		//passa pra view usar 
		$this->dadosEmpresa['paginacao'] =  $this->pagination->create_links();
		$this->dadosEmpresa['empresas'] = $this->Empresas_model->retornaListaEmpresas($config['per_page'], $inicio);
		
	    $this->load->view('includes/header');
	    $this->load->view('empresas/listar', $this->dadosEmpresa);
	    $this->load->view('includes/footer');
	    
	    
	}
	

	public function adicionarEmpresa(){
	    
	    if($this->input->post() != null)
	    {
	       	// Configura a validação dos inputs *mudar os campos requeridos
	    	$this->form_validation->set_rules('empresa', 'Empresa', 'trim|required|min_length[7]');
	    	$this->form_validation->set_rules('site', 'Site', 'trim|required|min_length[4]');
	    	$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
	    	$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required|max_length[300]');
	    	$this->form_validation->set_rules('responsavel', 'Responsável', 'trim|required|min_length[7]');
	    	$this->form_validation->set_rules('senha', 'Senha (Natural)', 'trim|required|min_length[7]');
	    	$this->form_validation->set_rules('senha2', 'Senha (Confirmação)', 'trim|required|matches[senha]');
	    
	    	if ($this->form_validation->run() == FALSE)
	    	{
	    		// Cria um flashdata com os erros de validação
	    		$this->session->set_flashdata('falhaCadastro', validation_errors());
				$this->load->view('includes/header');
			    $this->load->view('empresas/adicionar');
			    $this->load->view('includes/footer');
			}
            else
            {
				if($this->Empresas_model->adicionarEmpresa($this->input->post()))
				{
					$this->session->set_flashdata('formSucesso', 'Vaga adicionada com sucesso');
					redirect('empresas'); 
				}
				else
				{
					$this->session->set_flashdata('falhaCadastro', 'Falha ao adicionar ao banco');
					redirect('empresas');
				}
            }
	    }
	    else
	    {
	       	$this->load->view('includes/header');
			$this->load->view('empresas/adicionar');
			$this->load->view('includes/footer');
	    }
	
	}
}