<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index(){
		/* Verifica se uma sessão já esta em aberto, caso esteja redireciona */
		if($this->session->userdata('tipo_usuario') == 1){
				redirect(base_url().'login/admin');
				exit;
		}
		else if ($this->session->userdata('tipo_usuario') == 2){
			redirect(base_url().'login/empresa');
			exit;
		}
		$this->load->view('login');
	}
	
	public function logout(){
		/* Quebra a sessão e redireciona para página de login */
		$this->session->sess_destroy();
		$this->load->view('login');
	}
	
	public function verificaLogin()
	{
	    /* 
	    Caso "tipo_usuario" == 1 -> views/painel_adm
	    Caso "tipo_usuario" == 2 -> views/painel_empresa 
	    */
	    
	    // Salva o input do usuário nas variáveis usuario e senha
	    $email = $this->input->post('email');
	    $senha = $this->input->post('senha');
	    
	    // Configura as regras do formulário
		$this->form_validation->set_rules("email", "E-mail", "trim|required");
		$this->form_validation->set_rules("senha", "Senha", "trim|required");
		
		// Se o formulário não foi preenchido devidamente, é retornado um erro e a página de login novamente
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('falhaLogin','Usuário ou senha incorreto!');
			$this->load->view('login');
		}
		else{
			if($email == 'jamir@fatec.com' && $senha == '123'){
				$sess = array( 
				'nome' => 'Jamir',
				'tipo_usuario' => 1);
				$this->session->set_userdata($sess);

				redirect('login/admin');
			}
			// Manda o login e senha para a model e se coincidir login com senha é retornado a query
			$resultado = $this->Login_model->login($email, $senha);
			
			// Se o número de queries for igual a 1
			if(count($resultado) == 1){
				// configura a session
				$sess = array('id' => $resultado[0]->cd_Empresa, 
				'email' => $resultado[0]->ds_Email,
				'nome' => $resultado[0]->nm_Empresa,
				'tipo_usuario' => 2);
				$this->session->set_userdata($sess);

				redirect('login/empresa');
			}
			// Falha no login
			else{
				$this->session->set_flashdata('falhaLogin', 'Usuário ou senha incorreto!');
				$this->load->view('login');
			}
		}
	}
	
	// Verifica se o usuário tem permissão para acessar a página
	public function admin(){
	    if($this->session->userdata('tipo_usuario') == 1){
	    	$this->load->view('includes/header');
	        $this->load->view('painel_adm');
	        $this->load->view('includes/footer');
	    }
	    else{
	        echo 'você não tem permissão para acessar essa página';
	    }
	}
	
	public function empresa(){
	    if($this->session->userdata('tipo_usuario') == 2){
	    	$this->load->view('includes/header');
	        $this->load->view('painel_empresa');
	        $this->load->view('includes/footer');
	    }
	    else{
	        echo 'você não tem permissão para acessar essa página';
	    }
	}
}
