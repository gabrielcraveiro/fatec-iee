<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {
    
	function __construct() {
    	parent::__construct();
    	$this->load->helper(array('form', 'url'));
	}
	public function index(){
	    $this->load->view('upload_form', array('error' => ' ' ));
	}
	public function do_upload(){
		$config['upload_path'] = './assets/xlsx/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '1000';
        $this->upload->initialize($config);
		$this->load->library('upload', $config);
	    
		if ( ! $this->upload->do_upload('arquivo'))
		{
			$error = array('error' => $this->upload->display_errors());
			
		}	
		else
		{   
		    $data = array('upload_data' => $this->upload->data());
			$arquivo =$data['upload_data']['file_name'];
		    $file = $config['upload_path'];
		    $file .= $arquivo;
			$this->load->model('Alunos_model');
			
			$resultado= $this->Alunos_model->notas($file,'4');
		
			
		}
	}	
}
	

?>