<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vagas extends CI_Controller {
    
	public function __construct(){
		parent::__construct();
		$this->load->model('vagas_model');
		$this->load->library('pagination');
	}
	
	public function index(){
		
	    $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

		//Toda configuração de Paginação CodeIgniter
		$config['base_url'] = base_url().'vagas/index';
		$config['total_rows'] = $this->vagas_model->contaRegistros();
		$config['per_page'] = '10';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = 'Próximo';
		$config['prev_link'] = 'Anterior';
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;
		
	/*	$config['first_tag_open'] = "<div>";
		$config['first_tag_close'] = '</div>';
		$config['last_tag_open'] = "<div>";
		$config['last_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<b>';
		$config['cur_tag_close'] = '<b>'; */
		
		
		$this->pagination->initialize($config); 
		
		//passa pra view usar 
		$this->dadosVagas['paginacao'] =  $this->pagination->create_links();
		$this->dadosVagas['vagas'] = $this->vagas_model->retornaListaVagas($config['per_page'], $inicio);
		
	    $this->load->view('includes/header');
	    $this->load->view('vagas/listar', $this->dadosVagas);
	    $this->load->view('includes/footer');

	    
	}
	
	public function adicionarVaga(){
		// Verifica se é uma empresa logada
		if($this->session->userdata('tipo_usuario') != 2){
            redirect('login');
            exit;
        }
	    
	    // Se receber post valida e retorna sucesso ou falha de envio, se não receber post carrega o view vagas/adicionar
	    if($this->input->post() != null){
	    	// Configura a validação dos inputs
	    	$this->form_validation->set_rules('vaga', 'Vaga', 'trim|required');
	    	$this->form_validation->set_rules('jornada1', 'Jornada (entrada)', 'trim|required|exact_length[5]');
	    	$this->form_validation->set_rules('jornada2', 'Jornada (saída)', 'trim|required|exact_length[5]');
	    	$this->form_validation->set_rules('salario1', 'faixa salarial (mínimo)', 'trim|required|numeric');
	    	$this->form_validation->set_rules('salario2', 'faixa salarial (máximo)', 'trim|required|numeric');
	    	$this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
	    	$this->form_validation->set_rules('exigencia', 'Exigências', 'trim|required');
	    	$this->form_validation->set_rules('curso', 'Curso', 'trim|required');
	    	$this->form_validation->set_rules('classificacao', 'Classificação', 'trim|required');
	    	
	    	if ($this->form_validation->run() == FALSE){
	    		// Cria um flashdata com os erros de validação
	    		$this->session->set_flashdata('falhaCadastro', validation_errors());
				redirect('vagas/adicionarVaga');
			}
            else{
				if($this->vagas_model->adicionarVaga($this->input->post(), $this->session->userdata('id'))){
					
					$this->session->set_flashdata('formSucesso', 'Vaga adicionada com sucesso');
					redirect('vagas');
				}
				else{
					$this->session->set_flashdata('falhaCadastro', 'Falha ao adicionar ao banco');
					redirect('vagas');
				}
            }
	    	
	    	
	    	
	    }
	    else{
		    $this->load->view('includes/header');
		    $this->load->view('vagas/adicionar');
		    $this->load->view('includes/footer');	
	    }
	    
	}
	
	public function infoVaga($id){
		
		if(isset($id)){
			$dados['empresa'] = $this->vagas_model->getVaga($id);
			$dados['alunos'] = $this->vagas_model->getVagaAlunos($id);
			
			$this->load->view('includes/header');
		    $this->load->view('vagas/info', $dados);
		    $this->load->view('includes/footer');	
		}
		else{
			redirect('vagas');
		}
		
	}
	
}