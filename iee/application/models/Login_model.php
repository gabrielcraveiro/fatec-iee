<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_MODEL {
    
	function __construct() {
    	parent::__construct();
	}
	
    function login($email, $senha)
    {
        // Confere no banco de dados se o login e senha conferem
        // $query = $this->db->set($dados)->get_compiled_insert('mytable');
        return $this->db->get_where('TB_Empresas', array('ds_Email' => $email, 'cd_Senha' => $senha))->result();
    }
    
    function cadastrar($dados){
        $erro = FALSE;
        // Insere os dados na tabela alunos
        foreach ($dados as $dado){
            $query =  $this->db->set(array(
                'aluno' => $dado['aluno'],
                'ra' => $dado['ra'],
                'nascimento' => $dado['nascimento'],
                'email' => $dado['email'],
                'cpf' => $dado['cpf'],
                'curso' => $dado['curso'],
                'matriculado' => $dado['matriculado']
            ))->get_compiled_insert('alunos');
            if($this->db->query("$query ON DUPLICATE KEY UPDATE
                ra = VALUES(ra), curso = VALUES(curso)") == FALSE){
                $erro = TRUE;
            }
        }
        
        // Retorna falha ou sucesso
        if($erro){
             $this->session->set_flashdata('alert-danger', 'Falha no cadastro');
             echo 'falha';
        }
        else{
            $this->session->set_flashdata('alert-success', 'Cadastrado com sucesso!');
            echo 'sucesso';
        }
    }
    
    
    
}

