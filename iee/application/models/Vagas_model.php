<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vagas_model extends CI_MODEL {
    
	function __construct() {
    	parent::__construct();
	}
	
	function getVaga($id){
	    return $this->db->get_where('TB_Vagas', array('cd_vaga' => $id))->row();
	}
	
	function getVagaAlunos($id){
	    return $this->db->get_where('TB_Vaga_Alunos', array('cd_vaga' => $id))->result();
	}
	
    function adicionarVaga($dados, $idEmpresa){
        // O array para enviar para chamar o insert
        $query = array(
            'ds_TituloVaga' => $dados['vaga'],
            'ds_JornadaInicio' => $dados['jornada1'],
            'ds_JornadaFim'=> $dados['jornada2'],
            'vl_SalarioMin' => $dados['salario1'],
            'vl_SalarioMax' => $dados['salario2'],
            'ds_Vaga' => $dados['descricao'],
            'ds_Exigencias' => $dados['exigencia'],
            'cd_Empresa' => $idEmpresa,
            'ds_Classificacao' => $dados['classificacao'],
            'sg_Curso' => $dados['curso']
        );
        
        // Verifica se foi possivel inserior os dados no banco de dados
        if($this->db->insert('TB_Vagas', $query)){
            $this->vagaAlunos($dados);
            return TRUE;
        }
        else{
            return FALSE;
        }
        
    }
    
    function vagaAlunos($dados){
        
        // Gera lista de 10 melhores alunos com base nas informações da vaga.   
        
        $alunos = $this->db->query("
        
            SELECT h.ds_RaAluno, h.qt_Media, a.aluno as nm_Aluno, a.email as ds_Email
                FROM TB_Historico AS h
                INNER JOIN TB_Alunos AS a ON h.ds_RaAluno = a.ra
                WHERE h.ds_Classificacao =  '".$dados['classificacao']."'
                ORDER BY h.qt_Media DESC 
                LIMIT 0 , 10
        
        ")->result();

        $vaga = $this->db->query("SELECT * FROM TB_Vagas ORDER BY cd_Vaga DESC")->result();
        
        foreach($alunos as $a){
            
            $this->db->query("
                INSERT INTO TB_Vaga_Alunos VALUES('".$vaga[0]->cd_Vaga."','$a->ds_RaAluno', '$a->qt_Media', '$a->nm_Aluno', '$a->ds_Email')
            ");
            
        }
        
        //$this->db->query("INSERT INTO TB_Vaga_Alunos VALUES ('','".$alunos[0]->ra."')");
        
    }
    
    function retornaListaVagas($maximo, $inicio){
        return $this->db->get('TB_Vagas', $maximo, $inicio)->result();
    }
    
    function contaRegistros(){
        return $this->db->count_all_results('TB_Vagas');
    }
    
    
    
}

