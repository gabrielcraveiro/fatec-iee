<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Empresas_model extends CI_MODEL {
    
    function __construct() {
    	parent::__construct();
	}
    
	function retornaListaEmpresas($maximo, $inicio){
	    //pegando empresas cadastradas
        return $this->db->get('TB_Empresas', $maximo, $inicio)->result();
	}
    
    function contaRegistros(){
        return $this->db->count_all_results('TB_Empresas');
    }
    
    
    
    function adicionarEmpresa($dados){
        $query = array('nm_Empresa' => $dados['empresa'],
                       'ds_Site' => $dados['site'],
                       'ds_Email' => $dados['email'],
                       'ds_Empresa' => $dados['descricao'],
                       'nm_Responsavel' => $dados['responsavel'],
                       'cd_Senha' => $dados['senha']);
                       
        if($this->db->insert('TB_Empresas', $query)){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    
}
