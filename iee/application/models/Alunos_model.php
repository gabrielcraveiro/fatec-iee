<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alunos_model extends CI_MODEL {
    
	function __construct() {
    	parent::__construct();
    	$this->load->library('PHPExcel');
	}
	public function get($ra=0){
		$this->db->where('ra', $ra);
		$query = $this->db->get("TB_Alunos",1);
		if($query->num_rows() > 1){
			$row = $query->row();
			return $row;
		}
		else{
			$row="0";
			return $row;
		}
	}
	
	
	public function cadastrar_aluno($file,$label){
		
		$excelReader = PHPExcel_IOFactory::createReaderForFile($file);
		$excelObj = $excelReader -> load($file);
		$worksheet = $excelObj -> getSheet($label);
		$lastcollunm = $worksheet -> getHighestColumn();
		$highestColumnIndex = PhpExcel_Cell::columnIndexFromString($lastcollunm);
		$lastRow = $worksheet -> getHighestRow();
		for($row = 3; $row<=$lastRow; $row++){
		
			$ra = $worksheet -> getCell('B'.$row) -> getValue();
			$dados = array(
						'aluno' =>$worksheet -> getCell('C'.$row)->getValue(),
						'ra' => $worksheet -> getCell('B'.$row) -> getValue(),
						'nascimento' => date('Y-m-d', PHPExcel_Shared_date::ExcelToPHP($worksheet -> getCell('E'.$row)->getValue() )),
						'email' => $worksheet -> getCell('D'.$row)->getValue(),
						'cpf' => $worksheet -> getCell('A'.$row)->getValue(),
						'curso' => "SISTEMAS PARA INTERNET",
						'matriculado' => "1"
						);
			
			
			
			$valida= $this->Alunos_model->get($ra);
				if($valida == "0"){
					$this->db->insert('TB_Alunos',$dados);
					
					
				}
			
		
		}
		
		
	}
	
	public function get_notas($ra,$mat){
		$this->db->where('ds_RaAluno', $ra);
		$this->db->where('cd_Disciplina', $mat);
		$query = $this->db->get("TB_Notas",1);
		if($query->num_rows() > 0){
			$row = "1";
			return $row;
		}
		else{
			$row="0";
			return $row;
		}
		
	}
	
	
	public function notas($file,$label){
		$excelReader = PHPExcel_IOFactory::createReaderForFile($file);
		$excelObj = $excelReader -> load($file);
		$worksheet = $excelObj -> getSheet($label);
		$lastcollunm = $worksheet -> getHighestColumn();
		$highestColumnIndex = PhpExcel_Cell::columnIndexFromString($lastcollunm);
		$lastRow = $worksheet -> getHighestRow();
		$up=0;
			for ($row = 3; $row <= $lastRow; $row++) {
			echo "<tr><td>";
			for ($col = 5; $col <= $highestColumnIndex; ++$col) {

				if(!$worksheet->getCellByColumnAndRow($col,$row)->getValue() == NULL){
					$ra = $worksheet->getCell('B'.$row)->getValue();
					$disc = $worksheet->getCellByColumnAndRow($col,'2')->getValue();
					$dados=array(
						"ds_RaAluno" => $ra,
						"cd_Disciplina" => $disc,
						"qt_Nota" =>  $worksheet ->getCellByColumnAndRow($col,$row)->getValue()
						);
				if($dados['qt_Nota']>6){
					
					    $valida= $this->Alunos_model->get_notas($ra,$disc);
						if($valida == 0){
							$this->db->insert('TB_Notas',$dados);
							$veri="sucesso";
							$up++;
						}
				}
				}
			}
		}
			if($up==0){
				$titulo="Nenhuma nota cadastrada";	
			}
			else{
				$titulo=$up." notas cadastradas";
			}
			$this->setHistorico();
			$this->session->set_flashdata('sucessoCadastro', $titulo);
	
			redirect('alunos');
			
		
		
	}
	
	function setHistorico(){
        
        $tecnica = $this->db->query("
            SELECT n.ds_RaAluno, d.ds_Classificacao, ROUND(AVG(n.qt_Nota), 2) qt_Media
                FROM TB_Notas AS n
                INNER JOIN TB_Disciplinas AS d ON d.cd_Disciplina = n.cd_Disciplina
                WHERE d.ds_Classificacao = 'Técnica'
                GROUP BY ds_RaAluno
        ")->result();
        
        $negocios = $this->db->query("
            SELECT n.ds_RaAluno, d.ds_Classificacao, ROUND(AVG(n.qt_Nota), 2) qt_Media
                FROM TB_Notas AS n
                INNER JOIN TB_Disciplinas AS d ON d.cd_Disciplina = n.cd_Disciplina
                WHERE d.ds_Classificacao = 'Negócios'
                GROUP BY ds_RaAluno
        ")->result();
        
        $gestao = $this->db->query("
            SELECT n.ds_RaAluno, d.ds_Classificacao, ROUND(AVG(n.qt_Nota), 2) qt_Media
                FROM TB_Notas AS n
                INNER JOIN TB_Disciplinas AS d ON d.cd_Disciplina = n.cd_Disciplina
                WHERE d.ds_Classificacao = 'Gestão'
                GROUP BY ds_RaAluno
        ")->result();
        
        foreach($tecnica as $t){
    		$this->db->insert('TB_Historico', $t);
        }

        foreach($negocios as $n){
    		$this->db->insert('TB_Historico', $n);
        }
        
        foreach($gestao as $g){
    		$this->db->insert('TB_Historico', $g);
        }
        
    }
    
    function retornaListaVagas($maximo, $inicio){
        return $this->db->get('TB_Vagas', $maximo, $inicio)->result();
    }
    
    function contaRegistros(){
        return $this->db->count_all_results('TB_Vagas');
    }

	
}