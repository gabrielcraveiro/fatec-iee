 <!-- Fim Header --> 

  <!-- Conteudo -->

<div class="container">
    <br>
    <!-- ALERTA -->
    <?php if($this->session->flashdata('falhaCadastro')){ ?>
        <div class="alert alert-warning alert-dismissible show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?= $this->session->flashdata('falhaCadastro'); ?>
        </div>
    <?php } ?>
    <?php if($this->session->flashdata('formSucesso')){ ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?= $this->session->flashdata('formSucesso'); ?>
        </div>
    <?php } ?>
    
    <br>
    <h1>Lista de Vagas</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Vagas</th>
                <th>Descrição</th>
                <th>Editar</th>
                <th>Encerrar</th>
                <th>Excluir</th>
            </tr>
        </thead>
        
        <tbody>
        <?php foreach ($vagas as $v){ ?>
                <tr>
                    <td><a href="<?= base_url() ?>vagas/infoVaga/<?= $v->cd_Vaga ?>"> <?= $v->ds_TituloVaga ?></td>
                    <td><?= $v->ds_Vaga ?></td>
                    <td><a href='#'>Editar</a></td>
                    <td><a href='#'>Encerrar</a></td>
                    <td><a href='#'>Excluir</a></td>
                </tr>
        <?php } ?>
        </tbody>
    </table>
         <?php echo $paginacao; 
            echo "<p>Número de registros retornados nessa página: " . sizeof( $vagas ) . "</p>";
         ?>   
</div>

