  <!-- Fim Header --> 

  <!-- Conteudo -->

<div class="container">
    <br>
    <!-- ALERTA -->
    <?php if($this->session->flashdata('falhaCadastro')){ ?>
        <div class="alert alert-warning alert-dismissible show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?= $this->session->flashdata('falhaCadastro'); ?>
        </div>
    <?php } ?>
    
        
    <!-- CADASTRO MANUAL -->
    
    <h1>Cadastrar Vaga</h1>
    
    <form method="post" action="<?= base_url(); ?>vagas/adicionarVaga" id="manual">

        <div class="form-group">
            <label for="vaga">Titulo da Vaga</label>
            <input type="text" name="vaga" placeholder="Digite o Titulo da Vaga a ser ofertada" class="form-control"/>
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="inicio">Jornada das</label>
                <input type="text" name="jornada1" placeholder="--:--" class="jornada1 form-control" />
            </div>
            <div class="form-group col-sm-6">
                <label for="fim">ás</label>
                <input type="text" name="jornada2" placeholder="--:--" class="jornada2 form-control"/>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="salario1">Faixa Salarial</label>
                <input type="text" name="salario1" placeholder="Informe o Valor" class="salario1 form-control" />
            </div>
            <div class="form-group col-sm-6">
                <label for="salario2">a</label>
                <input type="text" name="salario2" placeholder="Informe o Valor" class="salario2 form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label for="descricao">Descrição da Empresa</label>
            <textarea name="descricao" placeholder="Fale sobre a Vaga" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="exigencia">Exigências</label>
            <textarea name="exigencia" placeholder="Digite as Exigências de Vaga" class="form-control"></textarea>
        </div>
        <!-- Curso -->
        <div class="form-group">
            <label for="curso">Curso</label>
            <select name="curso" class="form-control">
                <option value="ADS">ADS - Analise e Desenvolvimento de Sistema</option>
                <option value="GE">GE - Gestão Empresarial</option>
                <option value="GP">GP - Gestão Portuária</option>
                <option value="LOG">LOG - Logística</option>
                <option value="SI">SI - Sistemas para Internet</option>
            </select>
        </div>
        <!-- Classificação -->
        <div class="row">
            <div class="form-group col-md-6">
                <label for="classificacao">Classificação da Vaga</label>
                <select name="classificacao" class="form-control">
                    <option value="Técnica">Técnica</option>
                    <option value="Gestão">Gestão</option>
                    <option value="Negócios">Negócios</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="ciclo">A partir do </label>
                <select name="ciclo" class="form-control">
                    <option value="primeiroCiclo">Primeiro Ciclo</option>
                    <option value="segundoCiclo">Segundo Ciclo</option>
                    <option value="terceiroCiclo">Terceiro Ciclo</option>
                    <option value="quartoCiclo">Quarto Ciclo</option>
                    <option value="quintoCiclo">Quinto Ciclo</option>
                    <option value="sextoCiclo">Sexto Ciclo</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" value="Cadastrar" class="btn btn-primary"/>
        </div>
        
    </form>
            
</div>

