
  <!-- Fim Header --> 

  <!-- Conteudo -->
  <div class="container"> 


  <!-- Start: Content -->


      <div class="panel">

        <div class="panel-heading">

          <div class="panel-title"> <h1><?= $empresa->ds_TituloVaga ?> <small><?= $empresa->sg_Curso ?></small></h1></div>

        </div>

        <div class="panel-body">
          
          <div clas="row">
            
            <div class="col-lg-6 col-xs-12">
              <b>Descrição: </b>
              <p> <?= $empresa->ds_Vaga ?> </p>
              
              <b>Exigencias: </b>
              <p> <?= $empresa->ds_Exigencias ?> </p>
              
              <b>Jornada: </b>
              <p> <?= $empresa->ds_JornadaInicio ?> as <?= $empresa->ds_JornadaFim ?></p>
              
              <b>Salário: </b>
              <p> De R$ <?= $empresa->vl_SalarioMin ?>,00 a R$<?= $empresa->vl_SalarioMin ?>,00</p>
              <br>
              <a href="#" class="btn btn-primary btn-lg">Finalizar Vaga</a>
            </div>
            
            <div class="col-lg-6 col-xs-12">
              <ul class="list-group">
                <li class="list-group-item active">Alunos mais adequados a esta vaga</li>
                <?php foreach($alunos as $a){ ?>
              
                <li class="list-group-item"><?= $a->nm_Aluno ?></li>
                
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>

      </div>

    </div>
  
  </div>

<!-- Fim conteudo --> 

        
    