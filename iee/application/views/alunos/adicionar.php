  <!-- Fim Header --> 

  <!-- Conteudo -->

  <div id="main">
    <div class="container">

      <br>
      <!-- ALERTA -->
      <?php if($this->session->flashdata('falhaCadastro')){ ?>
          <div class="alert alert-warning alert-dismissible show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $this->session->flashdata('falhaCadastro'); ?>
          </div>
      <?php } ?>
      
      <?php if($this->session->flashdata('sucessoCadastro')){ ?>
          <div class="alert alert-success alert-dismissible show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?= $this->session->flashdata('sucessoCadastro'); ?>
          </div>
      <?php } ?>
      
      <!-- CARREGAR PLANILHA XLS -->
      
      <h1>Cadastrar por .Xls</h1>
      
      <?= form_open_multipart('upload/do_upload', 'id="xls"'); ?>
          <p>* Para cadastrar alunos a partir de um arquivo .xls este arquivo deve estar nos padrões corretos. Para ver um exemplo <a href="#">Clique aqui</a>.</p>
          <div class="form-group">
              <input type="file" name="arquivo" id="arquivo">
          </div>
          <div class="form-group">
              <input type="submit" value="Cadastrar" class="btn btn-primary"/>
          </div>
      </form>
    </div>