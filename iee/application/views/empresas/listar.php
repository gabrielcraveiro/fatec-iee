  <!-- Fim Header --> 

  <!-- Conteudo -->

<div class="container">
    <br>
    
    <!-- ALERTA -->
    <?php if($this->session->flashdata('formSucesso')){ ?>
        <div class="alert alert-warning alert-dismissible show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?= $this->session->flashdata('formSucesso'); ?>
        </div>
    <?php } ?>
    
    <h1>Lista de Empresas Cadastradas</h1>
    
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Empresas</th>
                <th>Responsável</th>
                <th>Vagas Abertas</th>
                <th>Editar</th>
                <th>Desabilitar</th>
            </tr>
        </thead>
        
        <tbody>
        <?php
            foreach ($empresas as $b){
                    echo "<tr>";
                        echo "<td>" . $b->nm_Empresa . "</td>";
                        echo "<td>" . $b->nm_Responsavel . "</td>";
                        echo "<td> </td>";
                        echo "<td><a href='#'>Editar</a></td>";
                        echo "<td><a href='#'>Desabilitar</a></td>";
                    echo "</tr>";
                    
            }
            
        ?>
        </tbody>
    </table>
         <?php echo $paginacao; 
            echo "<p>Número de registros retornados nessa página: " . sizeof( $empresas ) . "</p>";
         ?>   
</div>

