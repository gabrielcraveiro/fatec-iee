  <!-- Fim Header --> 

  <!-- Conteudo -->

<div class="container">
    <br>
    <!-- ALERTA -->
    <?php if($this->session->flashdata('falhaCadastro')){ ?>
        <div class="alert alert-warning alert-dismissible show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <?= $this->session->flashdata('falhaCadastro'); ?>
        </div>
    <?php } ?>
    
        
    <!-- CADASTRO MANUAL -->
    
    <h1>Cadastrar Empresa</h1>
    
    <form method="post" action="<?= base_url(); ?>empresas/adicionarEmpresa" id="manual">

        <div class="form-group">
            <label for="empresa">Nome da Empresa</label>
            <input type="text" name="empresa" placeholder="Digite o Nome da Empresa" class="form-control"/>
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="site">Site da Empresa</label>
                <input type="text" name="site" placeholder="Digite Site da Empresa" class="form-control siteEmpresa" value="http://" required/>
            </div>
            <div class="form-group col-sm-6">
                <label for="email">E-mail da Empresa</label>
                <input type="text" name="email" placeholder="Digite o E-mail da Empresa" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label for="descricao">Descrição da Empresa</label>
            <textarea name="descricao" placeholder="Fale um pouco sobre a Empresa" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="responsavel">Responsável</label>
            <input type="text" name="responsavel" placeholder="Digite o Nome do Responsável pela Empresa" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="senha">Senha de Acesso</label>
            <input type="password" name="senha" placeholder="Digite a Senha de Acesso da Empresa" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="senha">Digite a Senha novamente</label>
            <input type="password" name="senha2" placeholder="Digite a Senha de Acesso da Empresa" class="form-control"/>
        </div>
        <div class="form-group">
            <input type="submit" value="Cadastrar" class="btn btn-primary"/>
        </div>
        
    </form>
            
</div>
