<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>Fatec Baixada Santista - Rubens Lara — Santos-SP</title>
    	<meta name="description" content="A Fatec Rubens Lara oferece cursos de graduação em Análise e Desenvolvimento de Sistemas, Gestão Empresarial, Gestão Portuária, Logística e Sistemas para Internet">
    	<meta name="keywords" content="FATEC, Tecnologia, Rubens Lara, Santos, Vestibular, Análise e Desenvolvimento de Sistemas, Sistemas para Internet, Gestão Empresarial, Gestão Portuária, Logística, Superior, Centro Paula Souza">
    	<meta name="author" content="http://fatecrl.edu.br/site/humans.txt">
    	
    	<!-- Flat Icons -->
    	<link href="<?= site_url(); ?>assets/fonts/flaticon/flaticon.css" rel="stylesheet">
		
		<!-- Fatec Bootstrap -->
    	<link rel="stylesheet" type="text/css" media="all" href="https://fonts.googleapis.com/css?family=Roboto:400,500,900,300">
   		<link href="<?= site_url(); ?>assets/css/fatec-bootstrap.min.css" rel="stylesheet">
    	<!-- Style Fatec -->
    	<link href="<?= site_url(); ?>assets/css/style.css" rel="stylesheet">
    	
    	<!-- Favicons -->
    	<link rel="shortcut icon" type="image/ico" href="<?= site_url(); ?>assets/img/favicon.ico">
    	<link rel="shortcut icon" type="image/png" href="<?= site_url(); ?>assets/img/apple-touch-icon.png">
    	<link rel="apple-touch-icon" href="<?= site_url(); ?>assets/img/apple-touch-icon.png">
    	
    	<link rel="image_src" href="<?= site_url(); ?>assets/img/logo-fatec.png">
	    <meta property="og:locale" content="pt_br">
	    <meta property="og:title" content="<?= !empty($og_title) ? $og_title : 'Fatec Baixada Santista - Rubens Lara — Santos-SP'; ?>">
	    <meta property="og:description" content="<?= !empty($og_description) ? $og_description : 'FATEC, Tecnologia, Rubens Lara, Santos, Vestibular, Análise e Desenvolvimento de Sistemas, Sistemas para Internet, Gestão Empresarial, Gestão Portuária, Logística, Superior, Centro Paula Souza'; ?>">
	    <meta property="og:site_name" content="Fatec Baixada Santista - Rubens Lara — Santos-SP">
	    <meta property="og:type" content="website">
	    <meta property="og:image" content="<?= !empty($og_image) ? $og_image : site_url().'assets/img/logo-fatec.png'; ?>">

		
		
		<style type="text/css">
        :root #header + #content > #left > #rlblock_left,
        :root #content > #right > .dose > .dosesingle,
        :root #content > #center > .dose > .dosesingle {
            display: none !important;
        }
        .nav-wrapper {
            height: 50px;
        }
    </style>
	</head>

	<body class="page page-template-page-home-php site-fatec home index">
			
			    <header id="header" class="hidden-xs">
			        <div class="container">
			            <div class="row">
			                <div class="col-sm-4 col-md-4">
			                    <h1 class="hidden">Fatec Baixada Santista - Rubens Lara — Santos-SP</h1>
			                    <a href="http://fatecrl.edu.br/" title="Página Inicial Fatec Baixada Santista - Rubens Lara — Santos-SP">
			                        <img src="<?= site_url(); ?>assets/img/logo-fatec-corfatec.png" alt="Logo Fatec Baixada Santista - Rubens Lara — Santos-SP" class="img-responsive logo-fatecrl" height="90" width="165">
			                    </a>
			                </div>
			                <div class="col-sm-4 col-md-4">
			                    <h2 class="hidden">Centro Paula Souza</h2>
			                    <a href="http://www.cps.sp.gov.br/" title="Portal Centro Paula Souza (Link externo)" target="_blank">
			                        <img src="<?= site_url(); ?>assets/img/apoiadores/logo-centropaulasouza-corfatec.png" alt="Logo Centro Paula Souza" class="img-responsive logo-centropaulasouza" height="80" width="134">
			                    </a>
			                </div>
			                <div class="col-sm-4 col-md-4">
			                    <h2 class="hidden">Governo do Estado de São Paulo</h2>
			                    <a href="http://www.saopaulo.sp.gov.br/" title="Portal Governo do Estado de São Paulo (Link externo)" target="_blank">
			                        <img src="<?= site_url(); ?>assets/img/apoiadores/logo-govsp-corfatec.png" alt="Logo Governo do Estado de São Paulo" class="img-responsive logo-governoestadosp" height="50" width="243">
			                    </a>
			                </div>
			            </div>
			        </div>
			    </header>
			    
			    <div class="nav-wrapper">
			    	<nav id="navbar-main" class="navbar navbar-inverse">
				    	<div class="container">
	                		<div class="row">
								<ul class="nav navbar-nav">
	
							<li>
								<a href="<?= base_url() ?>login"><span class="glyphicon glyphicon-home"></span> Home </a>
							</li>
	
						
						<!-- MENU ADMIN -->	
						<?php if($this->session->userdata('tipo_usuario') == 1){ ?>
						<!--<li class="dropdown">
	
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-user" aria-hidden="true"></span> 
								Alunos 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="drop1">
								<li>
									<a href="<?php echo base_url();?>alunos/listarAlunos">
										<span class="glyphicon glyphicon-list" aria-hidden="true"></span> 
										Listar
									</a>
								</li>
								<li role="separator" class="divider"></li>
								<li>
									<a href="<?php echo base_url();?>cadastro">
										<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
										Adicionar
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>cadastro/cadastrarXls">
										<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
										Adicionar por .xls
									</a>
								</li>
							</ul>
						</li>-->
	
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-apple" aria-hidden="true"></span> 
								Empresas 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="drop1">
								<li>
									<a href="<?php echo base_url();?>empresas">
										<span class="glyphicon glyphicon-list" aria-hidden="true"></span> 
										Listar
									</a>
								</li>
								<li role="separator" class="divider"></li>
								<li>
									<a href="<?php echo base_url();?>empresas/adicionarEmpresa">
										<span class="glyphicon glyphicon-plus" aria-hidden="true">
										</span> Adicionar Empresa
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-apple" aria-hidden="true"></span> 
								Alunos 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="drop1">
								
								<li>
									<a href="<?php echo base_url();?>alunos">
										<span class="glyphicon glyphicon-plus" aria-hidden="true">
										</span> Adicionar Histórico
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-apple" aria-hidden="true"></span> 
								Vagas 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="drop1">
								<li>
									<a href="<?php echo base_url();?>vagas">
										<span class="glyphicon glyphicon-list" aria-hidden="true"></span> 
										Listar Vagas
									</a>
								</li>
							</ul>
						</li>
						<?php }else{ ?>
						
						<!-- MENU EMPRESA -->
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-apple" aria-hidden="true"></span> 
								Vagas 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="drop1">
								<li>
									<a href="<?php echo base_url();?>vagas">
										<span class="glyphicon glyphicon-list" aria-hidden="true"></span> 
										Listar Vagas
									</a>
								</li>
								<li role="separator" class="divider"></li>
								<li>
									<a href="<?php echo base_url();?>vagas/adicionarVaga">
										<span class="glyphicon glyphicon-plus" aria-hidden="true">
										</span> Adicionar Vaga
									</a>
								</li>
							</ul>
						</li>
						<?php } ?>
					</ul>
					
					
					
					
					
					<!-- lista toda do menu -->

					<ul class="nav navbar-nav pull-right">
						<li class="">
							<a href="<?= base_url()?>login/logout">
								<span class="glyphicon glyphicon-off" aria-hidden="true"></span> 
								Sair 
							</a>
						</li>
					</ul>
				</div>
			</div>
    	</nav>
    </div>

