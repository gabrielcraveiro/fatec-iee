    <footer id="footer">
        <div class="container">
            <div class="row copyright">
                <div class="col-xs-8 col-sm-8 col-md-5">
                    <p class="text-left">© 2016 <a href="http://fatecrl.edu.br/">Fatec Baixada Santista - Rubens Lara — Santos-SP</a>.</p>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-2">
                    <p class="text-center">
                        <a href="http://fatecrl.edu.br/" title="Página Inicial Fatec Baixada Santista - Rubens Lara — Santos-SP">
                            <img src="<?= site_url(); ?>assets/img/logo-fatec-corflight.png" alt="Logo Fatec Baixada Santista - Rubens Lara — Santos-SP" class="img-responsive logo-fatecrl" height="65" width="130">
                        </a>
                    </p>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-5">
                    <p class="text-right">Criado por <a href="http://fatecrl.edu.br/site/setores/equipe-web">Equipe WEB</a> Fatec Rubens Lara.</p>
                </div>
            </div>
        </div>
    </footer>
	<!-- Editar de Texto--> 

	<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.0.1/ckeditor.js"></script> 



	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<?= base_url(); ?>public/js/jquery.mask.min.js"></script>
	<script src="<?= base_url(); ?>static/js/script.js"></script>

	<!-- Latest compiled and minified JavaScript -->

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    
    <script src="<?= base_url() ?>public/js/script.js"></script>

<script>
        $(document).ready(function(){
            $('.jornada1').mask('00:00');
            $('.jornada2').mask('00:00');
        });
</script>
	</body>

</html>

