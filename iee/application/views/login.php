<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>Fatec Baixada Santista - Rubens Lara — Santos-SP</title>
    	<meta name="description" content="A Fatec Rubens Lara oferece cursos de graduação em Análise e Desenvolvimento de Sistemas, Gestão Empresarial, Gestão Portuária, Logística e Sistemas para Internet">
    	<meta name="keywords" content="FATEC, Tecnologia, Rubens Lara, Santos, Vestibular, Análise e Desenvolvimento de Sistemas, Sistemas para Internet, Gestão Empresarial, Gestão Portuária, Logística, Superior, Centro Paula Souza">
    	<meta name="author" content="http://fatecrl.edu.br/site/humans.txt">
    	
    	<!-- Flat Icons -->
    	<link href="<?= site_url(); ?>assets/fonts/flaticon/flaticon.css" rel="stylesheet">
		
		<!-- Fatec Bootstrap -->
    	<link rel="stylesheet" type="text/css" media="all" href="https://fonts.googleapis.com/css?family=Roboto:400,500,900,300">
   		<link href="<?= site_url(); ?>assets/css/fatec-bootstrap.min.css" rel="stylesheet">
    	<!-- Style Fatec -->
    	<link href="<?= site_url(); ?>assets/css/style.css" rel="stylesheet">
    	
    	<!-- Favicons -->
    	<link rel="shortcut icon" type="image/ico" href="<?= site_url(); ?>assets/img/favicon.ico">
    	<link rel="shortcut icon" type="image/png" href="<?= site_url(); ?>assets/img/apple-touch-icon.png">
    	<link rel="apple-touch-icon" href="<?= site_url(); ?>assets/img/apple-touch-icon.png">
    	
    	<link rel="image_src" href="<?= site_url(); ?>assets/img/logo-fatec.png">
	    <meta property="og:locale" content="pt_br">
	    <meta property="og:title" content="<?= !empty($og_title) ? $og_title : 'Fatec Baixada Santista - Rubens Lara — Santos-SP'; ?>">
	    <meta property="og:description" content="<?= !empty($og_description) ? $og_description : 'FATEC, Tecnologia, Rubens Lara, Santos, Vestibular, Análise e Desenvolvimento de Sistemas, Sistemas para Internet, Gestão Empresarial, Gestão Portuária, Logística, Superior, Centro Paula Souza'; ?>">
	    <meta property="og:site_name" content="Fatec Baixada Santista - Rubens Lara — Santos-SP">
	    <meta property="og:type" content="website">
	    <meta property="og:image" content="<?= !empty($og_image) ? $og_image : site_url().'assets/img/logo-fatec.png'; ?>">
		
	</head>

	<body class="page page-template-page-home-php site-fatec home index">
			
	    <header id="header" class="hidden-xs">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-4 col-md-4">
	                    <h1 class="hidden">Fatec Baixada Santista - Rubens Lara — Santos-SP</h1>
	                    <a href="http://fatecrl.edu.br/" title="Página Inicial Fatec Baixada Santista - Rubens Lara — Santos-SP">
	                        <img src="<?= site_url(); ?>assets/img/logo-fatec-corfatec.png" alt="Logo Fatec Baixada Santista - Rubens Lara — Santos-SP" class="img-responsive logo-fatecrl" height="90" width="165">
	                    </a>
	                </div>
	                <div class="col-sm-4 col-md-4">
	                    <h2 class="hidden">Centro Paula Souza</h2>
	                    <a href="http://www.cps.sp.gov.br/" title="Portal Centro Paula Souza (Link externo)" target="_blank">
	                        <img src="<?= site_url(); ?>assets/img/apoiadores/logo-centropaulasouza-corfatec.png" alt="Logo Centro Paula Souza" class="img-responsive logo-centropaulasouza" height="80" width="134">
	                    </a>
	                </div>
	                <div class="col-sm-4 col-md-4">
	                    <h2 class="hidden">Governo do Estado de São Paulo</h2>
	                    <a href="http://www.saopaulo.sp.gov.br/" title="Portal Governo do Estado de São Paulo (Link externo)" target="_blank">
	                        <img src="<?= site_url(); ?>assets/img/apoiadores/logo-govsp-corfatec.png" alt="Logo Governo do Estado de São Paulo" class="img-responsive logo-governoestadosp" height="50" width="243">
	                    </a>
	                </div>
	            </div>
	        </div>
	    </header>
			    

        <section id="login">
            <div class="container">        
                <form method="post" action="<?= base_url(); ?>login/verificaLogin">
                    
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" name="email" placeholder="Digite o nome do Usuário" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" placeholder="Digite a Senha" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Logar" class="btn btn-primary"/>
                    </div>
                    
                </form>
            </div>
        
        </section>
    </body>
    
</html>